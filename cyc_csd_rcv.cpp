//*******WAP to check the if a schedule is conflict serializable/cascadeless/recoverable 
//********from the Precedence Graph and do the topological sorting if DAG is there*******//

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <map>
#include <cmath>
#include <cstring>
#include <cassert>
#include <string>
#include <queue>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <ios>
#include <sstream>
#include <queue>
#include <list>
#include <stack>
#include <sys/time.h>
using namespace std;

void Precedence_Graph();
void findCycle();
bool isCyclic();
bool isCyclicUtil(int, bool*, bool*); 
void topologicalSort();
void topologicalSortUtil(int, bool*, stack<int>&);

void isCascade();
bool isRecoverable(bool, int, int, int, int);

vector< vector<string> >schedule_list;
vector<int>nodes;
vector< vector<int> >prec_graph;

//vector< vector< pair< pair<int,pair<string,string> > , pair<int,int> > > >E_prec_graph1;
typedef struct{
  public:
      int node_tj;
      string conflict;
      string data_item;
      int index_ti;
      int index_tj; 
}E_node;

vector< vector<E_node> >E_prec_graph; 



int main(int argc, char* argv[])
{
    string input_schedule_file = argv[1];
    

     ifstream in_schedule(input_schedule_file.c_str(),ios::in);
     if(!in_schedule)
     {cout<<input_schedule_file<<" Schedule file is not found check again!!"<<endl; exit(0);}
      cout<<"Input Schedule file is: "<<input_schedule_file<<endl;



                              //********************************   Read the input Schedule from the file   *******************************//



                 string str,word;
                 vector<string>vec;

                  while(getline(in_schedule,str))
		  {
                       
                       stringstream ss(str);

		               while (ss >> word)
				{
				    
				    vec.push_back(word);

				    if (ss.peek() == ' ')
					ss.ignore();
				}

                       schedule_list.push_back(vec);
                       vec.clear();
                      
                  }

                  //Display the transaction schedules
                  
                  for(int i=0;i<schedule_list.size();i++)
                  {
                      for(int j=0;j<schedule_list[i].size();j++)
                       cout<<schedule_list[i][j]<<" ";
                       cout<<endl;
                  }



          Precedence_Graph();
          findCycle();

          isCascade();

       return 0;
}


                         //*******************************************  PRECEDENCE GRAPH   ***********************************//



   void Precedence_Graph(){


        //Find the no. of nodes in the Precedence Graph
	                    
		for(int i=0;i<schedule_list.size();i++)
		{
		   int temp_node = atoi(schedule_list[i][1].c_str());

		                int j;
				   for(j=0;j<nodes.size();j++)
				   {
				       if(nodes[j] == temp_node)
					 break;
				   }
		         
		          if(j == nodes.size())
		            nodes.push_back(temp_node);
    
		} 

		sort(nodes.begin(),nodes.end()); //sort the nodes as per the index no.

		//Display nodes
		cout<<endl<<"The nodes are: "<<endl;
		for(int i=0;i<nodes.size();i++)
		{
		   cout<<nodes[i]<<" ";
 
		}
		cout<<endl<<"Number of nodes: "<<nodes.size()<<endl;
		if(nodes[0] != 0){
		prec_graph.resize(nodes[nodes.size()-1]+1); //Keeping the 0th row free
                E_prec_graph.resize(nodes[nodes.size()-1]+1);
                } 
		if(nodes[0] == 0){
		prec_graph.resize(nodes[nodes.size()-1]+1);
                E_prec_graph.resize(nodes[nodes.size()-1]+1);
                }
		
		vector<string>vec;
		vec.clear();

      
		for(int i=0;i<schedule_list.size();i++)
		{
                     if(schedule_list[i][0]=="C") continue;    
		   
		     int t_i = atoi(schedule_list[i][1].c_str()); //storing the source node
		        
			     for(int k=i+1;k<schedule_list.size();k++)
			     {
                                 if(schedule_list[k][0]=="C" && schedule_list[k][1] == schedule_list[i][1]) break;
                                 if(schedule_list[k][0]=="C" && schedule_list[k][1] != schedule_list[i][1]) continue; 

	                        //Detecting conflicting actions
				  if(schedule_list[i][0]=="R") //Read action
				  {
                                    
				     if(schedule_list[k][2] == schedule_list[i][2]) //same data item
				       if(schedule_list[k][1] != schedule_list[i][1]) //different transaction id
				         if(schedule_list[k][0] == "W") //one of the opn. is 'Write'
				          {
				            
				             int t_j = atoi(schedule_list[k][1].c_str());  
                                           
				             //t_i,t_j edge already doesn't exist                                 
				             if(!binary_search(prec_graph[t_i].begin(),prec_graph[t_i].end(),t_j)) 
				                prec_graph[t_i].push_back(t_j);
                                           
                                             //Create the Extended precedence graph
                                                E_node e_node;
                                                e_node.node_tj = t_j;
                                                e_node.conflict = "RW";
                                                e_node.data_item = schedule_list[i][2];
                                                e_node.index_ti = i;
                                                e_node.index_tj = k; 
                                                E_prec_graph[t_i].push_back(e_node);
				          }
                                    
	                             
				  }

                                 
				  if(schedule_list[i][0]=="W") //Write action
				  { 
				     if(schedule_list[k][0]=="C" && schedule_list[k][1] == schedule_list[i][1]) break;
				     if(schedule_list[k][0]=="C" && schedule_list[k][1] != schedule_list[i][1]) continue;
				     
				     if(schedule_list[k][2] == schedule_list[i][2]) //same data item
				       if(schedule_list[k][1] != schedule_list[i][1]) //different transaction id
		                        {
		                            
				            int t_j = atoi(schedule_list[k][1].c_str());  
   
		                             //t_i,t_j edge already doesn't exist                                 
				             if(!binary_search(prec_graph[t_i].begin(),prec_graph[t_i].end(),t_j)) 
				                prec_graph[t_i].push_back(t_j);

                                            
                                             //Create the Extended precedence graph
                                              E_node e_node;
                                                e_node.node_tj = t_j;
                                                if(schedule_list[k][0] == "R")
                                                  e_node.conflict = "WR";
                                                if(schedule_list[k][0] == "W")
                                                  e_node.conflict = "WW";
                                                e_node.data_item = schedule_list[i][2];
                                                e_node.index_ti = i;
                                                e_node.index_tj = k; 
                                                E_prec_graph[t_i].push_back(e_node);
                                                                                      
				        }
                                     
				  }
                                

			     }//end for   

		   
	 
	                      

		}//end for



               //Display the Precedence Graph
		cout<<endl<<"Precedence Graph:"<<endl;
		int i_node = nodes[0];        
		     
		for(int i=i_node;i<prec_graph.size();i++)
		{ 
		   if(!binary_search(nodes.begin(),nodes.end(),i))
		    continue;
		     
		   cout<<i<<" ";
		    for(int j=0;j<prec_graph[i].size();j++)
		    {
		        cout<<prec_graph[i][j]<<" ";
		    }

		    cout<<endl;
		}


                //Display the Extended Precedence Graph
		cout<<endl<<"Extended Precedence Graph:"<<endl;
	        i_node = nodes[0];        
		     
		for(int i=i_node;i<E_prec_graph.size();i++)
		{ 
		    if(!binary_search(nodes.begin(),nodes.end(),i))
		    continue;
		    
		    cout<<i<<" ";
		    for(int j=0;j<E_prec_graph[i].size();j++)
		    {
                       cout<<"[";
		       cout<<E_prec_graph[i][j].node_tj<<",";
                       cout<<E_prec_graph[i][j].conflict<<",";
                       cout<<E_prec_graph[i][j].data_item<<",";
                       cout<<E_prec_graph[i][j].index_ti<<",";
                       cout<<E_prec_graph[i][j].index_tj<<"]"; 
		    }

		    cout<<endl;
		}


	  
        }//End function




                              //************************** Detect CYCLES in the PRECEDENCE GRAPH ***************************//


  void findCycle()
  {
     if(isCyclic()) //cycle exists
       { 
           //cout<<endl<<"Precedence Graph contains cycle(s):\n Schedule is NOT Conflict Serializable"<<endl;
           cout<<"Conflict Serializable: NO"<<endl;
       }
     else           //cycle doesn't exist
       {

           //cout<<endl<<"Precedence Graph contains NO cycle:\n Schedule is Conflict Serializable"<<endl;
           cout<<"Conflict Serializable: YES"<<endl;

           topologicalSort(); // Topological sort

       }

  }
                               
  bool isCyclic()
  {
       
	       int V;
	       //cout<<V;
               int i_node = nodes[0];
               if(i_node == 0)
                 V = nodes[nodes.size()-1]+1;
               else
                 V = nodes[nodes.size()-1]+1;

	       // Mark all the vertices as not visited and not part of recursion stack
	       bool *visited = new bool[V];
	       bool *recStack = new bool[V];

	       for(int i = i_node; i < V; i++)
		    {
			visited[i] = false;
			recStack[i] = false;
		    }

         
               // Call the recursive helper function to detect cycle in different DFS trees
               
               bool cycle;
               for(int i = i_node; i < V; i++)
		  if (isCyclicUtil(i, visited, recStack))
                  {
                    cout<<"Cycle detected: ";
		      cycle = true;
                        int j;
         	          for(j = i; j < V; j++)
		             {
		               if(recStack[j] == true)
                                {
		                   cout<<j<<" ";
                                   recStack[j] = false; 
                                } 
		             }

                      cout<<i<<endl; 

                  }


	 
               if(cycle)
                return true;
               else
	        return false;
 

  }


   bool isCyclicUtil(int v, bool visited[], bool *recStack)
   {

		 if(visited[v] == false)
		    {
			// Mark the current node as visited and part of recursion stack
			visited[v] = true;
			recStack[v] = true;
		 
			// Recur for all the vertices adjacent to this vertex
			vector<int>::iterator i;
			for(i = prec_graph[v].begin(); i != prec_graph[v].end(); ++i)
			{
			    if ( !visited[*i] && isCyclicUtil(*i, visited, recStack) )
				return true;
			    else if (recStack[*i])
				return true;
			}
		 
		    }
		    recStack[v] = false;  // remove the vertex from recursion stack
		    return false;

   }


                                   //************************** Topological sort ***************************//



 void topologicalSort()
  {

       stack<int>Stack;
        int V;
	       
               int i_node = nodes[0];
               if(i_node == 0)
                 V = nodes[nodes.size()-1]+1;
               else
                 V = nodes[nodes.size()-1]+1;        
               //cout<<V;

           // Mark all the vertices as not visited
           bool *visited = new bool[V];
           for (int i = i_node; i < V; i++)
             visited[i] = false;


           // Call the recursive helper function to store Topological
           // Sort starting from all vertices one by one

           for (int i = i_node; i < V; i++)
	     if (visited[i] == false)
		topologicalSortUtil(i, visited, Stack);


           // Print contents of stack
           cout<<"The topological ordering is:"<<endl;
	    while (Stack.empty() == false)
	    {
	        if(binary_search(nodes.begin(),nodes.end(),Stack.top()))
	        {   
		
			cout << Stack.top() << " ";
			Stack.pop();		
		}
		 
		else
		 Stack.pop();
	    }

           cout<<endl; 

  }


  void topologicalSortUtil(int v, bool visited[], stack<int> &Stack)
  {
     
	      // Mark the current node as visited.
	      visited[v] = true;

              // Recur for all the vertices adjacent to this vertex
              vector<int>::iterator i;
              for (i = prec_graph[v].begin(); i != prec_graph[v].end(); ++i)
		if (!visited[*i])
		    topologicalSortUtil(*i, visited, Stack);


             // Push current vertex to stack which stores result
                Stack.push(v);


  } 


  void isCascade()
  {
       
       bool cascade_less = true; 
       bool CASCADE_LESS = true;
       bool RECOVERABLE = true;
       

       int V;
   		int i_node = nodes[0];
   		if(i_node == 0)
   			V = nodes[nodes.size()-1]+1;
		else
			V = nodes[nodes.size()-1]+1;


           //Test for "WR" conflict to find cascadeless schedule

           for(int i=i_node;i<E_prec_graph.size();i++)
           {
                  int j;
                   for(j=0;j<E_prec_graph[i].size();j++)
                   {           
	              if(E_prec_graph[i][j].conflict == "WR")
	              {
	                  int t_i = i;
                          int t_j = E_prec_graph[i][j].node_tj;
                          int Ti = E_prec_graph[i][j].index_ti;
                          int Tj = E_prec_graph[i][j].index_tj;  

                          stringstream ss_ti,ss_tj;
			       ss_ti << t_i;
			       string ti = ss_ti.str();
			       ss_tj << t_j;
			       string tj = ss_tj.str();

                          int k;
                          for(k=Ti+1;k<Tj;k++)
                          {
                            if(schedule_list[k][0] == "C" && schedule_list[k][1] == ti) // commit by ti b4 read by tj
                            break;
                          } 

                            if(k < Tj)
                              cascade_less = true;
                            else if(k == Tj){
                              cascade_less = false;
                              CASCADE_LESS = false; // to check for at least one ti tj pair is false.
                            } 

                              if(cascade_less == false) // check if the schedule is recoverable  
                              {
                                   //cout<<"call";
                                   if(!isRecoverable(cascade_less, Ti, Tj, t_i, t_j))
                                     { 
                                       RECOVERABLE = false;
                                       //cout<<"in func"<<schedule_list[i][1]<<schedule_list[j][1];
                                       break;
                                     }
                              }

          
	              }//end if

                   }//end for

           }//end for


           if(CASCADE_LESS == true && cascade_less == true){ //always true for all ti tj pairs.
             cout<<"\nCascade Rollback free: YES\n";
             cout<<"\nRecoverable: YES\n";
           }

           if(CASCADE_LESS == false){ // at least one ti tj pair is false.
             cout<<"\nCascade Rollback free: NO\n";

              if(RECOVERABLE)
                cout<<"\nRecoverable: YES\n";
              else
                cout<<"\nRecoverable: NO\n";
           }  


  }



bool isRecoverable(bool cascade_less, int Ti, int Tj, int ti, int tj)
 {
          
       int c_Ti=-1, c_Tj=-1;
       bool recoverable;
       stringstream ss_ti,ss_tj;
       ss_ti << ti;
       string t_i = ss_ti.str();
       ss_tj << tj;
       string t_j = ss_tj.str();
           
                  cout<<"Tid: "<<t_i<<" "<<t_j<<endl;
                                
		        for(int i=Ti+1;i<schedule_list.size();i++)
		        {
		           if(schedule_list[i][0] == "C" && schedule_list[i][1] == t_i)
		             { 
		                c_Ti = i; //timestamp for ti
		                break;
		             }
		        }

		        for(int j=Tj+1;j<schedule_list.size();j++)
		        {
		           if(schedule_list[j][0] == "C" && schedule_list[j][1] == t_j)
		             { 
		                c_Tj = j; //timestamp for tj
		                break;
		             }
		        }

                        cout<<"Index: "<<c_Ti<<" "<<c_Tj<<endl;
                                      
                           if(c_Ti < c_Tj)
                             recoverable = true;
                           if(c_Ti > c_Tj)
                             recoverable = false; 
                        

                        if(c_Ti !=-1 && c_Tj == -1)
                          recoverable = true; // undefined state.

                        if(c_Ti ==-1 && c_Tj != -1)
                          recoverable = false;

                        if(c_Ti == -1 && c_Tj == -1)
                          recoverable = false;

                 
                  return recoverable;

          

 }


















