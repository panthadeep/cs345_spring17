//*******WAP to check the if a schedule is conflict serializable from the Precedence Graph*******//
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <map>
#include <cmath>
#include <cstring>
#include <cassert>
#include <string>
#include <queue>
#include <stack>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <ios>
#include <sstream>
#include <queue>
#include <sys/time.h>
using namespace std;

void Precedence_Graph();
void findCycle();
bool isCyclic();
bool isCyclicUtil(int, bool*, bool*); 
void topological_sort();
void topologicalSortUtil(int, bool*, stack<int> &Stack);

vector< vector<string> >schedule_list;
vector<int>nodes;
vector< vector<int> >prec_graph;




int main(int argc, char* argv[])
{
    string input_schedule_file = argv[1];
    

     ifstream in_schedule(input_schedule_file.c_str(),ios::in);
     if(!in_schedule)
     {cout<<input_schedule_file<<" Schedule file is not found check again!!"<<endl; exit(0);}
      cout<<"Input Schedule file is: "<<input_schedule_file<<endl;



                              //********************************   Read the input Schedule from the file   *******************************//



                 string str,word;
                 vector<string>vec;

                  while(getline(in_schedule,str))
		  {
                       
                       stringstream ss(str);

		               while (ss >> word)
				{
				    
				    vec.push_back(word);

				    if (ss.peek() == ' ')
					ss.ignore();
				}

                       schedule_list.push_back(vec);
                       vec.clear();
                      
                  }

                  //Display the transaction schedules
                  
                  for(int i=0;i<schedule_list.size();i++)
                  {
                      for(int j=0;j<schedule_list[i].size();j++)
                       cout<<schedule_list[i][j]<<" ";
                       cout<<endl;
                  }



          Precedence_Graph();
          findCycle();

       return 0;
}


                         //*******************************************  PRECEDENCE GRAPH   ***********************************//



   void Precedence_Graph(){


        //Find the no. of nodes in the Precedence Graph
	                    
		for(int i=0;i<schedule_list.size();i++)
		{
		   int temp_node = atoi(schedule_list[i][1].c_str());

		                int j;
				   for(j=0;j<nodes.size();j++)
				   {
				       if(nodes[j] == temp_node)
					 break;
				   }
		         
		          if(j == nodes.size())
		            nodes.push_back(temp_node);
    
		} 

		sort(nodes.begin(),nodes.end()); //sort the nodes as per the index no.

		//Display nodes
		cout<<endl<<"The nodes are: "<<endl;
		for(int i=0;i<nodes.size();i++)
		{
		   cout<<nodes[i]<<" ";
 
		}
		cout<<endl<<"Number of nodes: "<<nodes.size()<<endl;
		if(nodes[0] != 0)
		prec_graph.resize(nodes.size()+1); //Keeping the 0th row free 
		if(nodes[0] == 0)
		prec_graph.resize(nodes.size());
		
		
		//*****************************temporary precedence graph***********************************//
		int i_node = nodes[0];
		
		for(int i=i_node;i<prec_graph.size();i++)
			for(int j=i_node;j<prec_graph.size();j++)
			{
				if(i == j)
					continue;
				prec_graph[i].push_back(j);
			}
		
		cout<<"\nTemporary precedence graph\n"; 
		for(int i=i_node;i<prec_graph.size();i++)
		{  
		  cout<<i<<" ";
		    for(int j=0;j<prec_graph[i].size();j++)
		    {
		        cout<<prec_graph[i][j]<<" ";
		    }

		    cout<<endl;
		}		
		
		
		//********************************************************************************************//
		
		//********************making the precedence graph*********************************************//
		
		vector<string>vec;
		vector<string>buff;
		vec.clear();
		bool present;
		int count = 0;
		for(int i = i_node;i<prec_graph.size();i++)
		{
			count+=1;
			for(int j = i_node;j<prec_graph.size();j++)
			{
				present = false;
				if(i == j)
					continue;
				for(int k =0;k<schedule_list.size();k++)
				{
				   vec = schedule_list[k];
				   if(i == atoi(vec[1].c_str()))
				   {
				   		
				   		buff = vec;
				   		for(int l = k+1;l<schedule_list.size();l++)
				   		{
				   			vec = schedule_list[l];
				   			if(j == atoi(vec[1].c_str()))
				   			{
				   				if(buff[0] == "R")
				   				{
				   					if(buff[2] == vec[2]) //same data item
									 if(vec[0] == "W") //one of the opn. is 'Write'
									  {
										present = true;
										break;
									  }
				   				}//end if
				   				
				   				if(buff[0]=="W") //Write action
								  {
								  
									 if(buff[2] == vec[2]) //same data item
									 {
											present = true;
											break;
									 }
				 
								  } //end if

				   			}//end if
				   			if(present==true)
				   				break;
				   		}//end for
				   		
				   	}//end if
				   	
				 if(present==true)
				   				break;
				   		
				}//end for
				if(present == true)
					continue;
				else
				{
				prec_graph[i].erase(std::remove(prec_graph[i].begin(), prec_graph[i].end(), j), prec_graph[i].end());
			
				}
			}
			
		}
		
		//**********************************************************************************************//
		
		
		
	     
		//Display the Precedence Graph
		cout<<endl<<"Precedence Graph:"<<endl;
		i_node = nodes[0];        
		     
		for(int i=i_node;i<prec_graph.size();i++)
		{  
		  cout<<i<<" ";
		    for(int j=0;j<prec_graph[i].size();j++)
		    {
		        cout<<prec_graph[i][j]<<" ";
		    }

		    cout<<endl;
		}


        }//End function




                              //************************** Detect CYCLES in the PRECEDENCE GRAPH ***************************//


  void findCycle()
  {
     if(isCyclic()) //cycle exists
       { 
           cout<<endl<<"Precedence Graph contains cycle(s):\n Schedule is NOT Conflict Serializable"<<endl;
       }
     else           //cycle doesn't exist
       {

           cout<<endl<<"Precedence Graph contains NO cycle:\n Schedule is Conflict Serializable"<<endl;
           topological_sort();
       }

  }
                               
  bool isCyclic()
  {
       
	       int V;
	       //cout<<V;
               int i_node = nodes[0];
               if(i_node == 0)
                 V = nodes.size();
               else
                 V = nodes.size() + 1;

	       // Mark all the vertices as not visited and not part of recursion stack
	       bool *visited = new bool[V];
	       bool *recStack = new bool[V];

	       for(int i = i_node; i < V; i++)
		    {
			visited[i] = false;
			recStack[i] = false;
		    }

         
               // Call the recursive helper function to detect cycle in different DFS trees

               for(int i = i_node; i < V; i++)
		  if (isCyclicUtil(i, visited, recStack))
		    return true;
	 
	       return false;
 

  }


   bool isCyclicUtil(int v, bool visited[], bool *recStack)
   {

		 if(visited[v] == false)
		    {
			// Mark the current node as visited and part of recursion stack
			visited[v] = true;
			recStack[v] = true;
		 
			// Recur for all the vertices adjacent to this vertex
			vector<int>::iterator i;
			for(i = prec_graph[v].begin(); i != prec_graph[v].end(); ++i)
			{
			    if ( !visited[*i] && isCyclicUtil(*i, visited, recStack) )
				return true;
			    else if (recStack[*i])
				return true;
			}
		 
		    }
		    recStack[v] = false;  // remove the vertex from recursion stack
		    return false;

   }
   
   	//****************************************topological sorting of the serializable schedule*********************************************//
   
   void topological_sort()
   {
   		int V;
   		int i_node = nodes[0];
   		if(i_node == 0)
   			V = nodes.size();
		else
			V = nodes.size()+1;
		
		stack<int> Stack;
		bool *visited = new bool[V];
		
		for(int i = i_node; i < V; i++)
			visited[i] = false;
			
		for(int i=i_node; i < V;i++)
			if(visited[i] == false)
				topologicalSortUtil(i, visited, Stack);	
				
		//Print contents of stack
		cout<<"\nTopological Sort\n";
		while (Stack.empty() == false)
		{
			cout << Stack.top() << " ";
			Stack.pop();
		}
   }

	void topologicalSortUtil(int v, bool* visited, stack<int> &Stack)
	{
		//mark the current node as visited 	
		visited[v] = true;
		
		// Recur for all the vertices adjacent to this vertex
		vector<int>::iterator i;
		for(i = prec_graph[v].begin(); i != prec_graph[v].end(); ++i)
		{
			if (!visited[*i])
				topologicalSortUtil(*i, visited, Stack);
		}		
		// Push current vertex to stack which stores result
		Stack.push(v);
	}























