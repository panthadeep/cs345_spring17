//*******WAP to check the if a schedule is conflict serializable from the Precedence Graph*******//
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <map>
#include <cmath>
#include <cstring>
#include <cassert>
#include <string>
#include <queue>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <ios>
#include <sstream>
#include <queue>
#include <sys/time.h>
using namespace std;


vector< vector<string> >schedule_list;
vector< vector<int> >prec_graph;


int main(int argc, char* argv[])
{
    string input_schedule_file = argv[1];
    

     ifstream in_schedule(input_schedule_file.c_str(),ios::in);
     if(!in_schedule)
     {cout<<input_schedule_file<<" Schedule file is not found check again!!"<<endl; exit(0);}
      cout<<"Input Schedule file is: "<<input_schedule_file<<endl;



                              //********************************   Read the input Schedule from the file   *******************************//



                 string str,word;
                 vector<string>vec;

                  while(getline(in_schedule,str))
		  {
                       
                       stringstream ss(str);

		               while (ss >> word)
				{
				    
				    vec.push_back(word);

				    if (ss.peek() == ' ')
					ss.ignore();
				}

                       schedule_list.push_back(vec);
                       vec.clear();
                      
                  }

                  //Display the transaction schedules
                  
                  for(int i=0;i<schedule_list.size();i++)
                  {
                      for(int j=0;j<schedule_list[i].size();j++)
                       cout<<schedule_list[i][j]<<" ";
                       cout<<endl;
                  }





                         //*******************************************  PRECEDENCE GRAPH   ***********************************//


                //Find the no. of nodes in the Precedence Graph
                vector<int>nodes;
                
                for(int i=0;i<schedule_list.size();i++)
                {
                   int temp_node = atoi(schedule_list[i][1].c_str());

                                int j;
				   for(j=0;j<nodes.size();j++)
				   {
				       if(nodes[j] == temp_node)
				         break;
				   }
                         
                          if(j == nodes.size())
                            nodes.push_back(temp_node);
    
                } 

                sort(nodes.begin(),nodes.end()); //sort the nodes as per the index no.

                //Display nodes
                cout<<endl<<"The nodes are: "<<endl;
                for(int i=0;i<nodes.size();i++)
                {
                   cout<<nodes[i]<<" ";
 
                }
                cout<<endl<<"Number of nodes: "<<nodes.size()<<endl;
                if(nodes[0] != 0)
                prec_graph.resize(nodes.size()+1); //Keeping the 0th row free 
                if(nodes[0] == 0)
                prec_graph.resize(nodes.size());
                
                vec.clear();
               
                for(int i=0;i<schedule_list.size();i++)
                {

		           for(int j=0;j<schedule_list[i].size();j++)
		           {
		              vec.push_back(schedule_list[i][j]);
		           }  

                     
                     int t_i = atoi(vec[1].c_str()); //storing the source node
                        
		             for(int k=i+1;k<schedule_list.size();k++)
		             {
	   
                                  //Detecting conflicting actions
		                  if(vec[0]=="R"); //Read action
		                  {
		                     if(schedule_list[k][2] == vec[2]) //same data item
		                       if(schedule_list[k][1] != vec[1]) //different transaction id
		                         if(schedule_list[k][0] == "W") //one of the opn. is 'Write'
		                          {
		                             
		                             int t_j = atoi(schedule_list[k][1].c_str());  

		                             //t_i,t_j edge already doesn't exist                                 
		                             if(!binary_search(prec_graph[t_i].begin(),prec_graph[t_i].end(),t_j)) 
		                                prec_graph[t_i].push_back(t_j);

		                          }
	 
		                  }


		                  if(vec[0]=="W") //Write action
		                  {
		                     if(schedule_list[k][2] == vec[2]) //same data item
		                       if(schedule_list[k][1] != vec[1]) //different transaction id
                                        {
                                            
		                            int t_j = atoi(schedule_list[k][1].c_str());  
   
                                             //t_i,t_j edge already doesn't exist                                 
		                             if(!binary_search(prec_graph[t_i].begin(),prec_graph[t_i].end(),t_j)) 
		                                prec_graph[t_i].push_back(t_j);
                                                                                    
		                        }
 
		                  }

		             }//end for   

                   
         
                     vec.clear();


                }//end for

             
                //Display the Precedence Graph
                cout<<endl<<"Precedence Graph:"<<endl;
                int i_node = nodes[0];        
                     
                for(int i=i_node;i<prec_graph.size();i++)
                {  
                  cout<<i<<" ";
                    for(int j=0;j<prec_graph[i].size();j++)
                    {
                        cout<<prec_graph[i][j]<<" ";
                    }

                    cout<<endl;
                }

    return 0;
}


























