#140101068_lab4.cpp 000777  001750  001750  00000011507 13057226102 015655  0                                                                                                    ustar 00shantanu543                     shantanu543                     000000  000000                                                                                                                                                                         #include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
using namespace std;
std::vector<int> trans;
std::vector<int> perf;
std::vector<int> co;
std::vector<int> cr;
std::vector<string> obj;
std::vector<std::vector<int> > ad_list;
std::vector<std::vector<int> > r_item;
std::vector<std::vector<int> > w_item;
std::vector<std::vector<string> > input;
int cascade = 1;
int recoverable = 1;
void splt(std::vector<string> &a,string s)
{
	int ind = 0;
	for (int i = 0; i < s.size(); ++i)
	{
		
		if(s.compare(i,1," ")==0)
		{
			a.push_back(s.substr(ind,i-ind));
			ind=i+1;
		}
	}
	if(ind!=0)
	{
		a.push_back(s.substr(ind,s.size()-ind));
	}
	else
	{
		a.push_back(s);
	}
	return;
}
int intify(string s) 
{
	int r=0;
	for (int i = 0; i < s.size(); ++i)
	{
		if(s[i]>57 || s[i]<48)
		{
			break;
		}
		r=r*10+(s[i]-48);
	}
	return r;
}
void pop_trans()
{
	for (int i = 0; i < input.size(); ++i)
	{
		if(input[i][0]!="C")
		{		
			int t = intify(input[i][1]);
			if(find(trans.begin(),trans.end(),t)==trans.end())
			{
				trans.push_back(t);
			}
		}
	}
}

void pop_obj()
{
	for (int i = 0; i < input.size(); ++i)
	{
		if(input[i][0]!="C")
		{		
			string t = input[i][2];
			if(find(obj.begin(),obj.end(),t)==obj.end())
			{
				obj.push_back(t);
			}
		}
	}
}

int obj_ind(string s)
{
	for (int i = 0; i < obj.size(); ++i)
	{
		if (s==obj[i])
		{
			return i;
		}
	}
}

int trans_ind(int n)
{
	for (int i = 0; i < trans.size(); ++i)
	{
		if (n==trans[i])
		{
			return i;
		}
	}
}

void ins_trans(int n,std::vector<std::vector<int> > &item,int ind)
{
	for (int i = 0; i < item[ind].size(); ++i)
	{
		if (item[ind][i]==n)
		{
			return;
		}
	}
	item[ind].push_back(n);
}

int find_ind(int n)
{
	for (int i = 0; i < co.size(); ++i)
	{
		if (co[i]==n)
		{
			return i;
		}
	}
}
void create_graph()
{
	for (int i = 0; i < input.size(); ++i)
	{
		//code to enter data in list
		if (input[i][0]=="R")
		{
			int tra = intify(input[i][1]);
			int ind1 = trans_ind(tra);
			int ind2 = obj_ind(input[i][2]);
			for (int i = 0; i < w_item[ind2].size(); ++i)
			{
				if(tra != w_item[ind2][i])
				{
					if (cr[trans_ind(w_item[ind2][i])]==0)
					{
						cascade = 0;
					}
					if(find_ind(w_item[ind2][i])>find_ind(tra))
					{
						recoverable = 0;
					}
					ins_trans(w_item[ind2][i],ad_list,ind1);
				}
			}
		}
		else if (input[i][0]=="W")
		{
			int tra = intify(input[i][1]);
			int ind1 = trans_ind(tra);
			int ind2 = obj_ind(input[i][2]);
			for (int i = 0; i < w_item[ind2].size(); ++i)
			{
				if(tra != w_item[ind2][i])
				{
					ins_trans(w_item[ind2][i],ad_list,ind1);
				}
			}
			for (int i = 0; i < r_item[ind2].size(); ++i)
			{
				if(tra != r_item[ind2][i])
				{
					ins_trans(r_item[ind2][i],ad_list,ind1);
				}
			}
		}
		else
		{
			// cout<<"here 1"<<endl;
			int c = intify(input[i][1]);
			int ind = trans_ind(c);
			cr[ind] = 1;
			// cout<<"here 2"<<endl;
		}
		//
		if (input[i][0]=="R")
		{
			int ind = obj_ind(input[i][2]);
			ins_trans(intify(input[i][1]),r_item,ind);
		}
		else if (input[i][0]=="W")
		{
			int ind = obj_ind(input[i][2]);
			ins_trans(intify(input[i][1]),w_item,ind);
		}
	}
}

void rem_from_list(int tr)
{
	for (int i = 0; i < ad_list.size(); ++i)
	{
		for (int j = 0; j < ad_list[i].size(); ++j)
		{
			if (ad_list[i][j]==tr)
			{
				ad_list[i].erase(ad_list[i].begin()+j);
				j--;
			}
		}
	}
}

void pop_co()
{
	for (int i = 0; i < input.size(); ++i)
	{
		if (input[i][0]=="C")
		{
			co.push_back(intify(input[i][1]));
		}
	}

}

int main(int argc, char const *argv[])
{
	
	string s=" ";
	ifstream f;
	f.open(argv[1]);
	while(getline(f,s))
	{
		std::vector<string> ags;
		splt(ags,s);
		input.push_back(ags);
		// cout<<ags.size()<<endl;
	}
	f.close();
	pop_trans();
	pop_obj();
	pop_co();
	ad_list.resize(trans.size());
	r_item.resize(obj.size());
	w_item.resize(obj.size());
	cr.resize(trans.size(),0);
	create_graph();
	
	while(perf.size()<trans.size())
	{
		int flag = 1;
		for (int i = 0; i < ad_list.size(); ++i)
		{
			if (ad_list[i].size()==0 && (find(perf.begin(),perf.end(),i)==perf.end()))
			{
				flag = 0;
				perf.push_back(i);
				int tr = trans[i];
				rem_from_list(tr);
				break;
			}
		}
		if (flag == 1)
		{
			cout<<"Conflict serializable : No"<<endl;
			
			if (recoverable==1)
			{
				cout<<"Recoverable: Yes"<<endl;
			}
			else
			{
				cout<<"Recoverable: No"<<endl;
			}
			if (cascade == 1)
			{
				cout<<"Cascaded roll back: Yes"<<endl;
			}
			else
			{
				cout<<"Cascaded roll back: No"<<endl;
			}
			return 0;
		}		
	}
	cout<<"Conflict serializable : Yes"<<endl;
	if (recoverable==1)
	{
		cout<<"Recoverable: Yes"<<endl;
	}
	else
	{
		cout<<"Recoverable: No"<<endl;
	}
	if (cascade == 1)
	{
		cout<<"Cascaded roll back: Yes"<<endl;
	}
	else
	{
		cout<<"Cascaded roll back: No"<<endl;
	}
	return 0;
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
