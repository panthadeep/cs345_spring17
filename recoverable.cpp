//*****************WAP to check if a schedule is cascade free and recoverable****************************************//
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <map>
#include <cmath>
#include <cstring>
#include <cassert>
#include <string>
#include <queue>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <ios>
#include <sstream>
#include <queue>
#include <sys/time.h>
using namespace std;

void check_cascadeless();
void check_recoverability();

vector< vector<string> >schedule_list;
vector<int>nodes;

int main(int argc, char* argv[])
{
	string input_schedule_file = argv[1];
    ifstream in_schedule(input_schedule_file.c_str(),ios::in);
    if(!in_schedule)
    {
    	cout<<input_schedule_file<<" Schedule file is not found check again!!"<<endl; exit(0);}
    	cout<<"Input Schedule file is: "<<input_schedule_file<<endl;
      
      //********************************   Read the input Schedule from the file   *******************************//
       string str,word;
       vector<string>vec;

		while(getline(in_schedule,str))
       {
            stringstream ss(str);

		    while (ss >> word)
			{
			    
			    vec.push_back(word);

			    if (ss.peek() == ' ')
				ss.ignore();
			}

           schedule_list.push_back(vec);
           vec.clear();
          
      }

      //Display the transaction schedules
      
      for(int i=0;i<schedule_list.size();i++)
      {
          for(int j=0;j<schedule_list[i].size();j++)
           cout<<schedule_list[i][j]<<" ";
           cout<<endl;
      }
      
      check_cascadeless();
                 
      return 0;           

}

void check_cascadeless()
{
	bool cascadeless = true;
	int t_i,t_j;
	vector<string>vec;
	int c_i = 100000;
	for(int i=0;i<schedule_list.size();i++)
	{
		if(schedule_list[i][0] == "W")
		{
			vec = schedule_list[i];
			t_i = i;
			for(int j=i+1;j<schedule_list.size();j++)
			{
				if(schedule_list[j][2] == vec[2])
					if(schedule_list[j][1] != vec[1])
						if(schedule_list[j][0] == "R")
						{
							t_j = j;
							for(int k = i+1;k<schedule_list.size();k++)
							{
								if(schedule_list[k][0] == "C" and schedule_list[k][1] == vec[1])
								{
									c_i = k;
									break;
								}
							}
							if(c_i<t_j)
								cascadeless = true;
							else
								{
									cascadeless = false;
									cout<<"\nSchedule is not cascade free\n";
									check_recoverability();
									exit(0);
								}
						}
							
			}
		}
	}
	if(cascadeless == true)
	{
		cout<<"\nSchedule is cascadefree\n";
		cout<<"\nSchedule is recoverable\n";
	}
}
void check_recoverability()
{
	int c_i = 100000, c_j = 100000;
	bool recoverable = false;
	for(int i = 0;i<schedule_list.size();i++)
	{
		vector<string>vec;
		if(schedule_list[i][0] == "W")
		{
			vec = schedule_list[i];
			for(int j = i+1;j<schedule_list.size();j++)
			{
				if(schedule_list[j][2] == vec[2])
					if(schedule_list[j][1] != vec[1])
						if(schedule_list[j][0] == "R")
						{
								for(int k = i+1;k<schedule_list.size();k++)
								{
									if(schedule_list[k][0] == "C" and schedule_list[k][1] == vec[1])
									{
										c_i = k;
										break;
									}							
								}
								for(int k = j+1;k<schedule_list.size();k++)
								{
									if(schedule_list[k][0] == "C" and schedule_list[k][1] == schedule_list[j][1])
									{
										c_j = k;
										break;
									}
								}
								if(c_i<c_j)
								{
									recoverable = true;
								}
								else
								{
									recoverable = false;
									cout<<"\nSchedule is not recoverable\n";
									exit(0);
								}
						}
			}
		}
	}
	if(recoverable == true)
	{
		cout<<"\nSchedule is recoverable\n";
		exit(0);
	}
}
